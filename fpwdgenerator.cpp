#include "fpwdgenerator.h"
#include <QByteArray>
#include "fpwdconstants.h"
#include "keccak.h"
#include "math.h"

fpwdGenerator::fpwdGenerator()
{
}

double fpwdGenerator::evaluateEntropy(QString _phrase)
{
    if (_phrase.isEmpty())
        return 0;

    bool syms[7];
    int symVariants = 0;

    for (int i = 0; i < 7; i++)
        syms[i] = false;

    foreach (QChar curChar, _phrase)
    {
        for (int i = 0; i < fpwdConstants::ascii_syms.length(); i++)
        {
            int start = fpwdConstants::ascii_syms.at(i).second;
            int length = fpwdConstants::ascii_syms.at(i).first;
            int end = start + length;
            if (curChar >= start && curChar <= end && !syms[i])
            {
                syms[i] = true;
                symVariants += length;
            }
        }
    }

    return _phrase.length() * log2(symVariants);
}

QByteArray fpwdGenerator::getPassword(QString _hash_base, int _password_length, bool _syms[4])
{
    QString pre_hash[8];
    QByteArray pwd[7];
    QByteArray pwd_ba;
    uint8_t md[8][64];
    int filled = 0;
    int index[7];
    int index_special = 0;
    int index_sym = 0;

    // Add pre-defined salt to each source
    for (int j = 0; j < 8; j++)
        pre_hash[j] = _hash_base + fpwdConstants::salts[j];

    // Extract SHA-3 hash for each source:
    // * 7 sources for symbols (a-z, A-Z, 0-9, four special ranges)
    // * 1 source for shuffling
    for (int j = 0; j < 8; j++)
        keccak((uint8_t *)pre_hash[j].toStdString().c_str(), pre_hash[j].length(), md[j], 64);

    // Convert SHA-3 hashes to appropriate ASCII symbols
    for (int j = 0; j < _password_length; j++)
        for (int k = 0; k < 7; k++)
            pwd[k].append(md[k][j] % fpwdConstants::ascii_syms[k].first
                          + fpwdConstants::ascii_syms[k].second);

    // Join generated ASCII symbols according to selected symbol options

    for (int j = 0; j < 7; j++)
        index[j] = 0;
    if (_syms[0] || _syms[1] || _syms[2] || _syms[3])
    {
        while (filled < _password_length)
        {
            if (_syms[index_sym])
            {
                if (index_sym == 3)
                {
                    // Special symbols are interleaved as they are located in separate ASCII ranges
                    pwd_ba.append(pwd[index_sym + index_special].
                            at(index[index_sym + index_special]++));
                    index_special++;
                    if (index_special > 3)
                        index_special = 0;
                } else
                    pwd_ba.append(pwd[index_sym].at(index[index_sym]++));
                filled++;
            }
            index_sym++;
            if (index_sym > 3)
                index_sym = 0;
        }

        // Shuffle joined ASCII symbols using Fisher-Yates algorithm according to extra hash
        for (int j = _password_length - 1; j >= 1; j--)
        {
            int k = md[7][j] % (j + 1);
            char tmp = pwd_ba[j];
            pwd_ba[j] = pwd_ba[k];
            pwd_ba[k] = tmp;
        }
    } else
        pwd_ba.clear();

    return pwd_ba;
}
