fastpwd
=======

Hash-based (SHA-3) Qt password generator and manager.

Description
-----------

fastpwd generates password based on master password (private and never stored) and resource information.

Resource information is:

* access protocol
* user login
* server name or address
* port
* resource path
* password version
* password symbol sets
* password length

Compiling
---------

### Prerequisites

* qmake (tested with 3.0)
* Qt (tested with 5.3.1)
* make (tested with GNU Make 4.0)
* C++ compiler (tested with Clang 3.4.2)

### Compiling

In source tree root type the following command to generate make file:

`qmake`

Then compile the whole project:

`make -jN`

where N is machine CPU count. As a result `fastpwd` executable should be produced.

Distribution and Contribution
-----------------------------

fastpwd is provided under terms and conditions of GPLv3.
See file "COPYING" for details.
Mail any suggestions, bugreports and comments to me: oleksandr@natalenko.name.
