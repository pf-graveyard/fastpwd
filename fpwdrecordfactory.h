#ifndef FPWDRECORDFACTORY_H
#define FPWDRECORDFACTORY_H

/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include <QString>
#include "fpwdrecord.h"
#include <QList>

class fpwdRecordFactory
{
public:
    fpwdRecordFactory();
    ~fpwdRecordFactory();
    void readFromFile(QString _filename);
    fpwdRecord *addRecord(QString _protocol,
                   QString _login,
                   QString _server,
                   int _port,
                   QString _resource,
                   int _version,
                   int _length,
                   bool _syms_az,
                   bool _syms_AZ,
                   bool _syms_09,
                   bool _syms_special);
    fpwdRecord *getRecord(int _id);
    void saveRecordToFile(QString _filename, int _id);
    void removeRecordFromFile(QString _filename, int _id);
    void deleteRecord(int _id);
    QList<fpwdRecord *> *getRecords();
private:
    QList<fpwdRecord *> *records;
    int lastId;
};

#endif // FPWDRECORDFACTORY_H
