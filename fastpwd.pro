#-------------------------------------------------
#
# Project created by QtCreator 2014-08-27T00:06:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fastpwd
TEMPLATE = app

QMAKE_CC = clang
QMAKE_CXX = clang++

SOURCES += main.cpp\
        mainwindow.cpp \
    keccak.cpp \
    fpwdlineedit.cpp \
    fpwdrecord.cpp \
    fpwdrecordfactory.cpp \
    fpwdpushbutton.cpp \
    fpwdspinbox.cpp \
    fpwdcombobox.cpp \
    fpwdconstants.cpp \
    fpwdcheckbox.cpp \
    aboutwindow.cpp \
    fpwdgenerator.cpp

HEADERS  += mainwindow.h \
    keccak.h \
    fpwdlineedit.h \
    fpwdrecord.h \
    fpwdrecordfactory.h \
    fpwdpushbutton.h \
    fpwdspinbox.h \
    fpwdcombobox.h \
    fpwdconstants.h \
    fpwdcheckbox.h \
    aboutwindow.h \
    fpwdgenerator.h

FORMS    += mainwindow.ui \
    aboutwindow.ui

target.path = /usr/bin/
INSTALLS += target

OTHER_FILES += \
    fastpwd.png

RESOURCES += \
    fastpwd.qrc
