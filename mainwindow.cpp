/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fpwdlineedit.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "fpwdrecord.h"
#include "fpwdrecordfactory.h"
#include "fpwdpushbutton.h"
#include "fpwdspinbox.h"
#include "fpwdcombobox.h"
#include "fpwdconstants.h"
#include "fpwdcheckbox.h"
#include <QClipboard>
#include <QDir>
#include <QMessageBox>
#include "aboutwindow.h"
#include <QProgressBar>
#include <math.h>
#include "fpwdgenerator.h"
#include <sys/mman.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if (!QDir(fpwdConstants::dirCfg).exists())
        QDir().mkdir(fpwdConstants::dirCfg);
    records = new fpwdRecordFactory();
    records->readFromFile(fpwdConstants::fileURLs);
    foreach (fpwdRecord *cur, *records->getRecords())
    {
        this->addToTable(cur);
        this->updateTablePassword(cur->getId());
    }
    this->uiAbout = new AboutWindow(this);
    mlock(this->ui->lineEditMasterPassword,
          sizeof(*this->ui->lineEditMasterPassword));
    mlock(this->ui->lineEditMasterPasswordConfirmation,
          sizeof(*this->ui->lineEditMasterPasswordConfirmation));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::commonActionAdd()
{
    fpwdRecord *newRecord = this->records->addRecord("https",
                                                     "",
                                                     "",
                                                     443,
                                                     "/",
                                                     1,
                                                     20,
                                                     true,
                                                     true,
                                                     true,
                                                     true);
    this->addToTable(newRecord);
    this->updateTablePassword(newRecord->getId());
}

void MainWindow::addToTable(fpwdRecord *_record)
{
    fpwdPushButton *cellDelete = new fpwdPushButton(_record->getId());
    cellDelete->setText("X");
    QObject::connect(cellDelete,
                     SIGNAL(clicked()),
                     this,
                     SLOT(cellDeleteOnClick()));
    cellDelete->setFixedWidth(30);

    fpwdPushButton *cellCopy = new fpwdPushButton(_record->getId());
    cellCopy->setText("Copy");
    QObject::connect(cellCopy,
                     SIGNAL(clicked()),
                     this,
                     SLOT(cellCopyOnClick()));
    cellCopy->setFixedWidth(40);

    fpwdLineEdit *cellProtocol = new fpwdLineEdit(_record->getId());
    cellProtocol->setText(_record->getProtocol());
    cellProtocol->setFrame(false);
    cellProtocol->setCursorPosition(0);
    cellProtocol->setFixedWidth(60);
    QObject::connect(cellProtocol,
                     SIGNAL(textEdited(const QString &)),
                     this,
                     SLOT(cellProtocolOnTextEdit(const QString &)));

    fpwdLineEdit *cellLogin = new fpwdLineEdit(_record->getId());
    cellLogin->setText(_record->getLogin());
    cellLogin->setFrame(false);
    cellLogin->setCursorPosition(0);
    QObject::connect(cellLogin,
                     SIGNAL(textEdited(const QString &)),
                     this,
                     SLOT(cellLoginOnTextEdit(const QString &)));

    QWidget *cellPasswordWidget = new QWidget();
    QVBoxLayout *cellPasswordLayout = new QVBoxLayout();
    cellPasswordLayout->setContentsMargins(0, 0, 0, 0);
    cellPasswordLayout->setSpacing(0);
    fpwdLineEdit *cellPassword = new fpwdLineEdit(_record->getId());
    mlock(cellPassword, sizeof(*cellPassword));
    cellPassword->setEchoMode(fpwdLineEdit::Password);
    cellPassword->setReadOnly(true);
    cellPassword->setFrame(false);
    cellPassword->setCursorPosition(0);
    QObject::connect(cellPassword,
                     SIGNAL(doubleClicked()),
                     this,
                     SLOT(cellPasswordOnDoubleClick()));
    QProgressBar *cellPasswordStrength = new QProgressBar();
    cellPasswordStrength->setFixedHeight(5);
    cellPasswordStrength->setTextVisible(false);
    cellPasswordLayout->addWidget(cellPassword);
    cellPasswordLayout->addWidget(cellPasswordStrength);
    cellPasswordWidget->setLayout(cellPasswordLayout);

    fpwdLineEdit *cellServer = new fpwdLineEdit(_record->getId());
    cellServer->setText(_record->getServer());
    cellServer->setCursorPosition(0);
    cellServer->setFrame(false);
    QObject::connect(cellServer,
                     SIGNAL(textEdited(const QString &)),
                     this,
                     SLOT(cellServerOnTextEdit(const QString &)));

    fpwdSpinBox *cellPort = new fpwdSpinBox(_record->getId());
    cellPort->setMinimum(0);
    cellPort->setMaximum(65535);
    cellPort->setValue(_record->getPort());
    cellPort->setFrame(false);
    QObject::connect(cellPort,
                     SIGNAL(valueChanged(int)),
                     this,
                     SLOT(cellPortOnValueChange(int)));

    fpwdLineEdit *cellResource = new fpwdLineEdit(_record->getId());
    cellResource->setText(_record->getResource());
    cellResource->setFrame(false);
    QObject::connect(cellResource,
                     SIGNAL(textEdited(const QString &)),
                     this,
                     SLOT(cellResourceOnTextEdit(const QString &)));
    cellResource->setFixedWidth(120);

    fpwdSpinBox *cellVersion = new fpwdSpinBox(_record->getId());
    cellVersion->setMinimum(1);
    cellVersion->setValue(_record->getVersion());
    cellVersion->setFrame(false);
    QObject::connect(cellVersion,
                     SIGNAL(valueChanged(int)),
                     this,
                     SLOT(cellVersionOnValueChange(int)));

    fpwdSpinBox *cellLength = new fpwdSpinBox(_record->getId());
    cellLength->setMinimum(1);
    cellLength->setMaximum(64);
    cellLength->setValue(_record->getLength());
    cellLength->setFrame(false);
    QObject::connect(cellLength,
                     SIGNAL(valueChanged(int)),
                     this,
                     SLOT(cellLengthOnValueChange(int)));

    QWidget *cell_az_widget = new QWidget();
    QWidget *cell_AZ_widget = new QWidget();
    QWidget *cell_09_widget = new QWidget();
    QWidget *cell_special_widget = new QWidget();
    QHBoxLayout *cell_az_layout = new QHBoxLayout();
    QHBoxLayout *cell_AZ_layout = new QHBoxLayout();
    QHBoxLayout *cell_09_layout = new QHBoxLayout();
    QHBoxLayout *cell_special_layout = new QHBoxLayout();
    fpwdCheckBox *cell_az = new fpwdCheckBox(_record->getId());
    fpwdCheckBox *cell_AZ = new fpwdCheckBox(_record->getId());
    fpwdCheckBox *cell_09 = new fpwdCheckBox(_record->getId());
    fpwdCheckBox *cell_special = new fpwdCheckBox(_record->getId());
    cell_az->setChecked(_record->getSyms_az());
    cell_AZ->setChecked(_record->getSyms_AZ());
    cell_09->setChecked(_record->getSyms_09());
    cell_special->setChecked(_record->getSyms_special());
    QObject::connect(cell_az,
                     SIGNAL(stateChanged(int)),
                     this,
                     SLOT(cellSyms_az_change(int)));
    QObject::connect(cell_AZ,
                     SIGNAL(stateChanged(int)),
                     this,
                     SLOT(cellSyms_AZ_change(int)));
    QObject::connect(cell_09,
                     SIGNAL(stateChanged(int)),
                     this,
                     SLOT(cellSyms_09_change(int)));
    QObject::connect(cell_special,
                     SIGNAL(stateChanged(int)),
                     this,
                     SLOT(cellSyms_special_change(int)));
    cell_az_layout->addWidget(cell_az);
    cell_AZ_layout->addWidget(cell_AZ);
    cell_09_layout->addWidget(cell_09);
    cell_special_layout->addWidget(cell_special);
    cell_az_layout->setAlignment(cell_az, Qt::AlignCenter);
    cell_AZ_layout->setAlignment(cell_AZ, Qt::AlignCenter);
    cell_09_layout->setAlignment(cell_09, Qt::AlignCenter);
    cell_special_layout->setAlignment(cell_special, Qt::AlignCenter);
    cell_az_widget->setLayout(cell_az_layout);
    cell_AZ_widget->setLayout(cell_AZ_layout);
    cell_09_widget->setLayout(cell_09_layout);
    cell_special_widget->setLayout(cell_special_layout);

    ui->tableWidgetURIs->insertRow(0);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_DELETE, cellDelete);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_COPY, cellCopy);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_PROTOCOL, cellProtocol);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_LOGIN, cellLogin);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_PASSWORD, cellPasswordWidget);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_SERVER, cellServer);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_PORT, cellPort);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_RESOURCE, cellResource);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_VERSION, cellVersion);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_SYMS_az, cell_az_widget);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_SYMS_AZ, cell_AZ_widget);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_SYMS_09, cell_09_widget);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_SYMS_special, cell_special_widget);
    ui->tableWidgetURIs->setCellWidget(0, fpwdConstants::CELL_LENGTH, cellLength);

    ui->tableWidgetURIs->resizeColumnsToContents();
    ui->tableWidgetURIs->resizeRowsToContents();
}

void MainWindow::deleteFromTable(int _id)
{
    for (int i = 0; i < ui->tableWidgetURIs->rowCount(); i++)
    {
        fpwdPushButton *currentButton = (fpwdPushButton *)(ui->tableWidgetURIs->cellWidget(i, fpwdConstants::CELL_DELETE));
        if (currentButton->getId() == _id)
        {
            for (int j = 0; j < ui->tableWidgetURIs->columnCount(); j++)
                ui->tableWidgetURIs->removeCellWidget(i, j);
            ui->tableWidgetURIs->removeRow(i);
            return;
        }
    }
}

void MainWindow::on_actionExit_triggered()
{
    exit(0);
}

void MainWindow::cellPasswordOnDoubleClick()
{
    fpwdLineEdit *currentLineEdit = (fpwdLineEdit *)(QObject::sender());
    if (currentLineEdit->echoMode() == fpwdLineEdit::Password)
    {
        QMessageBox::StandardButton reply =
                QMessageBox::question(this,
                                      "Show password",
                                      "Are you sure?",
                                      QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
            currentLineEdit->setEchoMode(fpwdLineEdit::Normal);
    } else
        currentLineEdit->setEchoMode(fpwdLineEdit::Password);
    currentLineEdit->setSelection(0, 0);
    currentLineEdit->setCursorPosition(0);
}

void MainWindow::cellDeleteOnClick()
{
    fpwdPushButton *currentButton = (fpwdPushButton *)(QObject::sender());
    int id = currentButton->getId();
    this->deleteFromTable(id);
    this->records->deleteRecord(id);
    this->records->removeRecordFromFile(fpwdConstants::fileURLs, id);
}

void MainWindow::cellCopyOnClick()
{
    fpwdPushButton *sender = (fpwdPushButton *)(QObject::sender());
    for (int i = 0; i < ui->tableWidgetURIs->rowCount(); i++)
    {
        fpwdPushButton *currentButton = (fpwdPushButton *)(ui->tableWidgetURIs->cellWidget(i, fpwdConstants::CELL_COPY));
        if (currentButton->getId() == sender->getId())
        {
            QString pwd = ((fpwdLineEdit *)(((QWidget *)ui->tableWidgetURIs->cellWidget(i, fpwdConstants::CELL_PASSWORD))->
                                            findChild<fpwdLineEdit *>()))->text();
            QClipboard *clipboard = QApplication::clipboard();
            clipboard->setText(pwd);
        }
    }
}

void MainWindow::cellProtocolOnTextEdit(const QString _text)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setProtocol(_text);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellLoginOnTextEdit(const QString _text)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setLogin(_text);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellServerOnTextEdit(const QString _text)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setServer(_text);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellResourceOnTextEdit(const QString _text)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setResource(_text);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellVersionOnValueChange(int _i)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setVersion(_i);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellPortOnValueChange(int _i)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setPort(_i);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellLengthOnValueChange(int _i)
{
    fpwdLineEdit *sender = ((fpwdLineEdit *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setLength(_i);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellSyms_az_change(int _i)
{
    fpwdCheckBox *sender = ((fpwdCheckBox *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setSyms_az(_i == Qt::Checked);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellSyms_AZ_change(int _i)
{
    fpwdCheckBox *sender = ((fpwdCheckBox *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setSyms_AZ(_i == Qt::Checked);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellSyms_09_change(int _i)
{
    fpwdCheckBox *sender = ((fpwdCheckBox *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setSyms_09(_i == Qt::Checked);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::cellSyms_special_change(int _i)
{
    fpwdCheckBox *sender = ((fpwdCheckBox *)(QObject::sender()));
    int id = sender->getId();
    records->getRecord(id)->setSyms_special(_i == Qt::Checked);

    this->records->saveRecordToFile(fpwdConstants::fileURLs, id);
    this->updateTablePassword(id);
}

void MainWindow::on_actionAdd_triggered()
{
    this->commonActionAdd();
}

void MainWindow::updateTablePassword(int _id)
{
    bool syms[4];
    QString part[7];
    int length;
    QString pre_hash_base;
    QString pwd;

    int row;
    for (int i = 0; i < ui->tableWidgetURIs->rowCount(); i++)
    {
        fpwdPushButton *anchor = ((fpwdPushButton *)(ui->tableWidgetURIs->cellWidget(i, fpwdConstants::CELL_DELETE)));
        if (anchor->getId() == _id)
        {
            row = i;
            break;
        }
    }

    // Get checked symbol options
    for (int j = 0; j < 4; j++)
        syms[j] = ui->tableWidgetURIs->
                cellWidget(row, j + fpwdConstants::CELL_SYMS_az)->
                findChild<fpwdCheckBox *>()->isChecked();
    // Get desired password length
    length = ((fpwdSpinBox *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_LENGTH))->value();
    // Get URL parts
    part[0] = ((fpwdLineEdit *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_PROTOCOL))->text();
    part[1] = ((fpwdLineEdit *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_LOGIN))->text();
    part[2] = ((fpwdLineEdit *)ui->lineEditMasterPassword)->text();
    part[3] = ((fpwdLineEdit *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_SERVER))->text();
    part[4] = ((fpwdSpinBox *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_PORT))->text();
    part[5] =  ((fpwdLineEdit *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_RESOURCE))->text();
    part[6] =  ((fpwdSpinBox *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_VERSION))->text();
    // Consolidate URL parts
    for (int j = 0; j < 7; j++)
        pre_hash_base += part[j];
    // Add length to base URL (salt #1)
    pre_hash_base += QString::number(length);
    // Add symbol options to base URL (salt #2)
    for (int j = 0; j < 4; j++)
        pre_hash_base += QString::number(syms[j]);

    // Set password box value
    fpwdLineEdit *pwdWidget =
            ((QWidget *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_PASSWORD))->
            findChild<fpwdLineEdit *>();
    pwd = QString(fpwdGenerator::getPassword(pre_hash_base, length, syms));
    pwdWidget->setText(pwd);
    pwdWidget->setCursorPosition(0);

    // Draw password strength
    QProgressBar *pwdStrength =
            ((QWidget *)ui->tableWidgetURIs->cellWidget(row, fpwdConstants::CELL_PASSWORD))->
            findChild<QProgressBar *>();
    QPalette p = pwdStrength->palette();
    double percentage;
    int r;
    int g;
    if (!pwd.isEmpty())
    {
        double strength1 = fpwdGenerator::evaluateEntropy(pwd);
        if (strength1 > fpwdConstants::maxPasswordStrength)
            strength1 = fpwdConstants::maxPasswordStrength;
        double strength2 = fpwdGenerator::evaluateEntropy(part[2]);
        if (strength2 > fpwdConstants::maxPasswordStrength)
            strength2 = fpwdConstants::maxPasswordStrength;
        double strength = 0.5 * (strength1 + strength2);

        percentage = strength / fpwdConstants::maxPasswordStrength;
        r = 255 * (1.0 - percentage);
        g = 255 * percentage;
    } else
    {
        percentage = 1.0;
        r = 255;
        g = 0;
    }
    p.setColor(QPalette::Highlight, QColor(r, g, 0));
    pwdStrength->setValue((int)floor(100.0 * percentage));
    pwdStrength->setPalette(p);
}

void MainWindow::on_lineEditMasterPassword_textEdited(const QString &arg1)
{
    (void)arg1;
    this->acceptMasterPassword();
}

void MainWindow::on_lineEditMasterPasswordConfirmation_textEdited(const QString &arg1)
{
    (void)arg1;
    this->acceptMasterPassword();
}

void MainWindow::acceptMasterPassword()
{
    QString master = ui->lineEditMasterPassword->text();
    QString confirmation = ui->lineEditMasterPasswordConfirmation->text();
    QPalette p = ui->lineEditMasterPasswordConfirmation->palette();
    if (master != confirmation)
        p.setColor(QPalette::Base, QColor(Qt::red));
    else
    {
        foreach (fpwdRecord *cur, *records->getRecords())
            this->updateTablePassword(cur->getId());
        p.setColor(QPalette::Base, QColor(Qt::white));
    }
    ui->lineEditMasterPasswordConfirmation->setPalette(p);
}

void MainWindow::on_actionAbout_triggered()
{
    this->uiAbout->show();
}

void MainWindow::on_lineEditFilter_textEdited(const QString &arg1)
{
    for (int i = 0; i < ui->tableWidgetURIs->rowCount(); i++)
    {
        fpwdLineEdit *currentServer = ((fpwdLineEdit *)(ui->tableWidgetURIs->cellWidget(i, fpwdConstants::CELL_SERVER)));
        if (currentServer->text().indexOf(arg1) != -1)
            ui->tableWidgetURIs->showRow(i);
        else
            ui->tableWidgetURIs->hideRow(i);
    }
}

