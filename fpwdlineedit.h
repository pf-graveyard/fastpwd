#ifndef FPWDLINEEDIT_H
#define FPWDLINEEDIT_H

/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include <QLineEdit>

class fpwdLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    fpwdLineEdit(int _id);
    int getId();
private:
    int id;

signals:
    void doubleClicked();

protected:
    void mouseDoubleClickEvent(QMouseEvent *e);

public slots:

};

#endif // FPWDLINEEDIT_H
