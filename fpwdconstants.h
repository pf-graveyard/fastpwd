#ifndef FPWDCONSTANTS_H
#define FPWDCONSTANTS_H

/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include <QStringList>
#include <QList>
#include <QPair>

class fpwdConstants
{
public:
    fpwdConstants();
    static const QStringList salts;
    static const QList< QPair<int, int> > ascii_syms;
    static const double maxPasswordStrength;
    static const QString fileURLs;
    static const QString fileProtocols;
    static const QString dirCfg;
    static enum
    {
        CELL_DELETE = 0,
        CELL_COPY = 1,
        CELL_PROTOCOL = 2,
        CELL_LOGIN = 3,
        CELL_PASSWORD = 4,
        CELL_SERVER = 5,
        CELL_PORT = 6,
        CELL_RESOURCE = 7,
        CELL_VERSION = 8,
        CELL_SYMS_az = 9,
        CELL_SYMS_AZ = 10,
        CELL_SYMS_09 = 11,
        CELL_SYMS_special = 12,
        CELL_LENGTH = 13
    } UiCell;
};

#endif // FPWDCONSTANTS_H
