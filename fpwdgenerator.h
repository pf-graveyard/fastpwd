#ifndef FPWDGENERATOR_H
#define FPWDGENERATOR_H

#include <QByteArray>
#include <QString>

class fpwdGenerator
{
public:
    fpwdGenerator();
    static QByteArray getPassword(QString _hash_base, int _password_length, bool _syms[4]);
    static double evaluateEntropy(QString _phrase);
};

#endif // FPWDGENERATOR_H
