#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include <QMainWindow>
#include <QTableWidget>
#include "fpwdlineedit.h"
#include <QList>
#include "fpwdrecord.h"
#include "fpwdrecordfactory.h"
#include "aboutwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    AboutWindow *uiAbout;
    fpwdRecordFactory *records;
    void commonActionAdd();
    void addToTable(fpwdRecord *_record);
    void deleteFromTable(int _id);
    void updatePasswords();
    void updateTablePassword(int _id);
    void acceptMasterPassword();

private slots:
    void on_actionExit_triggered();
    void on_actionAdd_triggered();
    void cellPasswordOnDoubleClick();
    void cellDeleteOnClick();
    void cellCopyOnClick();
    void cellProtocolOnTextEdit(const QString _text);
    void cellLoginOnTextEdit(const QString _text);
    void cellServerOnTextEdit(const QString _text);
    void cellResourceOnTextEdit(const QString _text);
    void cellPortOnValueChange(int _i);
    void cellVersionOnValueChange(int _i);
    void cellLengthOnValueChange(int _i);
    void cellSyms_az_change(int _i);
    void cellSyms_AZ_change(int _i);
    void cellSyms_09_change(int _i);
    void cellSyms_special_change(int _i);
    void on_lineEditMasterPassword_textEdited(const QString &arg1);
    void on_actionAbout_triggered();
    void on_lineEditFilter_textEdited(const QString &arg1);
    void on_lineEditMasterPasswordConfirmation_textEdited(const QString &arg1);
};

#endif // MAINWINDOW_H
