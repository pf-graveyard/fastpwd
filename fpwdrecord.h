#ifndef FPWDRECORD_H
#define FPWDRECORD_H

/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include <QString>

class fpwdRecord
{
public:
    fpwdRecord(int _id,
               QString _protocol,
               QString _login,
               QString _server,
               int _port,
               QString _resource,
               int _version,
               int _length,
               bool _syms_az,
               bool _syms_AZ,
               bool _syms_09,
               bool _syms_special);
    QString getProtocol();
    void setProtocol(QString _protocol);
    QString getLogin();
    void setLogin(QString _login);
    int getId();
    void setId(int _id);
    QString getServer();
    void setServer(QString _server);
    int getPort();
    void setPort(int _port);
    QString getResource();
    void setResource(QString _resource);
    int getVersion();
    void setVersion(int _version);
    int getLength();
    void setLength(int _length);
    bool getSyms_az();
    void setSyms_az(bool _state);
    bool getSyms_AZ();
    void setSyms_AZ(bool _state);
    bool getSyms_09();
    void setSyms_09(bool _state);
    bool getSyms_special();
    void setSyms_special(bool _state);
private:
    int id;
    QString protocol;
    QString login;
    QString server;
    int port;
    QString resource;
    int version;
    int length;
    bool syms_az;
    bool syms_AZ;
    bool syms_09;
    bool syms_special;
};

#endif // FPWDRECORD_H
