/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include "fpwdrecord.h"

fpwdRecord::fpwdRecord(int _id,
                       QString _protocol,
                       QString _login,
                       QString _server,
                       int _port,
                       QString _resource,
                       int _version,
                       int _length,
                       bool _syms_az,
                       bool _syms_AZ,
                       bool _syms_09,
                       bool _syms_special)
{
    this->id = _id;
    this->protocol = _protocol;
    this->login = _login;
    this->server = _server;
    this->port = _port;
    this->resource = _resource;
    this->version = _version;
    this->length = _length;
    this->syms_az = _syms_az;
    this->syms_AZ = _syms_AZ;
    this->syms_09 = _syms_09;
    this->syms_special = _syms_special;
}

QString fpwdRecord::getProtocol()
{
    return this->protocol;
}

void fpwdRecord::setProtocol(QString _protocol)
{
    this->protocol = _protocol;
}

QString fpwdRecord::getLogin()
{
    return this->login;
}

void fpwdRecord::setLogin(QString _login)
{
    this->login = _login;
}

int fpwdRecord::getId()
{
    return this->id;
}

void fpwdRecord::setId(int _id)
{
    this->id = _id;
}

QString fpwdRecord::getServer()
{
    return this->server;
}

void fpwdRecord::setServer(QString _server)
{
    this->server = _server;
}

int fpwdRecord::getPort()
{
    return this->port;
}

void fpwdRecord::setPort(int _port)
{
    this->port = _port;
}

QString fpwdRecord::getResource()
{
    return this->resource;
}

void fpwdRecord::setResource(QString _resource)
{
    this->resource = _resource;
}

int fpwdRecord::getVersion()
{
    return this->version;
}

void fpwdRecord::setVersion(int _version)
{
    this->version = _version;
}

int fpwdRecord::getLength()
{
    return this->length;
}

void fpwdRecord::setLength(int _length)
{
    this->length = _length;
}

bool fpwdRecord::getSyms_az()
{
    return this->syms_az;
}

void fpwdRecord::setSyms_az(bool _state)
{
    this->syms_az = _state;
}

bool fpwdRecord::getSyms_AZ()
{
    return this->syms_AZ;
}

void fpwdRecord::setSyms_AZ(bool _state)
{
    this->syms_AZ = _state;
}

bool fpwdRecord::getSyms_09()
{
    return this->syms_09;
}

void fpwdRecord::setSyms_09(bool _state)
{
    this->syms_09 = _state;
}

bool fpwdRecord::getSyms_special()
{
    return this->syms_special;
}

void fpwdRecord::setSyms_special(bool _state)
{
    this->syms_special = _state;
}
