/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include "fpwdconstants.h"
#include <QStringList>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
#include <QStandardPaths>
#else
#include <QDesktopServices>
#endif

#include <QDir>
#include "fpwdgenerator.h"

fpwdConstants::fpwdConstants()
{
}

const QStringList fpwdConstants::salts = QStringList()
        << "az"
        << "AZ"
        << "09"
        << "special1"
        << "special2"
        << "special3"
        << "special4"
        << "fisher yates shuffle";

const QList< QPair<int, int> > fpwdConstants::ascii_syms = QList< QPair<int, int> >()
        << QPair<int, int>(26, 97)  // a..z
        << QPair<int, int>(26, 65)  // A..Z
        << QPair<int, int>(10, 48)  // 0..9
        << QPair<int, int>(15, 33)  // !../
        << QPair<int, int>(7, 58)   // :..@
        << QPair<int, int>(6, 91)   // [..`
        << QPair<int, int>(4, 123); // {..~

const double fpwdConstants::maxPasswordStrength =
        fpwdGenerator::evaluateEntropy("aA0!:[]{}bB1cC2dD3eE");

const QString fpwdConstants::dirCfg =
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
        QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation) +
#else
        QDesktopServices::storageLocation(QDesktopServices::DataLocation) +
#endif
        QString(QDir::separator()) +
        "fastpwd";
const QString fpwdConstants::fileURLs = fpwdConstants::dirCfg +
        QString(QDir::separator()) +
        "urls.conf";
const QString fpwdConstants::fileProtocols = fpwdConstants::dirCfg +
        QString(QDir::separator()) +
        "protocols.conf";
