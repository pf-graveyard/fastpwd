/**
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * fastpwd — hash-based (SHA-3) Qt password generator and manager
 */

#include "fpwdrecordfactory.h"

#include <QString>
#include "fpwdrecord.h"
#include <QList>
#include <QSettings>
#include "fpwdconstants.h"

fpwdRecordFactory::fpwdRecordFactory()
{
    this->records = new QList<fpwdRecord *>();
    this->lastId = 0;
}

fpwdRecordFactory::~fpwdRecordFactory()
{
    delete this->records;
}

fpwdRecord *fpwdRecordFactory::addRecord(QString _protocol,
               QString _login,
               QString _server,
               int _port,
               QString _resource,
               int _version,
               int _length,
               bool _syms_az,
               bool _syms_AZ,
               bool _syms_09,
               bool _syms_special)
{
    fpwdRecord *newRecord = new fpwdRecord(this->lastId++,
                                           _protocol,
                                           _login,
                                           _server,
                                           _port,
                                           _resource,
                                           _version,
                                           _length,
                                           _syms_az,
                                           _syms_AZ,
                                           _syms_09,
                                           _syms_special);
    this->records->append(newRecord);
    return newRecord;
}

fpwdRecord *fpwdRecordFactory::getRecord(int _id)
{
    for (int i = 0; i < this->records->length(); i++)
    {
        fpwdRecord *currentRecord = this->records->at(i);
        if (currentRecord->getId() == _id)
            return currentRecord;
    }
    return NULL;
}

void fpwdRecordFactory::deleteRecord(int _id)
{
    for (int i = 0; i < this->records->length(); i++)
    {
        fpwdRecord *currentRecord = this->records->at(i);
        if (currentRecord->getId() == _id)
        {
            this->records->removeOne(currentRecord);
            delete currentRecord;
            return;
        }
    }
}

QList<fpwdRecord *> *fpwdRecordFactory::getRecords()
{
    return this->records;
}

void fpwdRecordFactory::readFromFile(QString _filename)
{
    QSettings conf(_filename, QSettings::IniFormat);
    QStringList groups = conf.childGroups();
    foreach (const QString &currentGroup, groups)
    {
        conf.beginGroup(currentGroup);
        QStringList keys = conf.allKeys();

        QString protocol;
        QString login;
        QString server;
        int port;
        QString resource;
        int version;
        int length;
        bool syms_az;
        bool syms_AZ;
        bool syms_09;
        bool syms_special;
        foreach (const QString &currentKey, keys)
        {
            QString value = conf.value(currentKey).toString();
            if (currentKey == "protocol")
            {
                protocol = value;
            } else if (currentKey == "login")
            {
                login = value;
            } else if (currentKey == "server")
            {
                server = value;
            } else if (currentKey == "port")
            {
                port = value.toInt();
            } else if (currentKey == "resource")
            {
                resource = value;
            } else if (currentKey == "version")
            {
                version = value.toInt();
            } else if (currentKey == "length")
            {
                length = value.toInt();
            } else if (currentKey == "syms_az")
            {
                syms_az = value == "true";
            } else if (currentKey == "syms_AZ")
            {
                syms_AZ = value == "true";
            } else if (currentKey == "syms_09")
            {
                syms_09 = value == "true";
            } else if (currentKey == "syms_special")
            {
                syms_special = value == "true";
            }
        }

        fpwdRecord *newRecord = this->addRecord(protocol,
                        login,
                        server,
                        port,
                        resource,
                        version,
                        length,
                        syms_az,
                        syms_AZ,
                        syms_09,
                        syms_special);
        newRecord->setId(currentGroup.toInt());
        if (currentGroup.toInt() >= this->lastId)
            this->lastId = currentGroup.toInt() + 1;

        conf.endGroup();
    }
}

void fpwdRecordFactory::saveRecordToFile(QString _filename, int _id)
{
    QSettings conf(_filename, QSettings::IniFormat);

    int index = -1;
    for (int i = 0; i < this->records->length(); i++)
        if (this->records->at(i)->getId() == _id)
        {
            index = i;
            break;
        }

    if (index >= 0)
    {
        fpwdRecord *currentRecord = this->records->at(index);
        conf.beginGroup(QString::number(_id));
        conf.setValue("protocol", currentRecord->getProtocol());
        conf.setValue("login", currentRecord->getLogin());
        conf.setValue("server", currentRecord->getServer());
        conf.setValue("port", currentRecord->getPort());
        conf.setValue("resource", currentRecord->getResource());
        conf.setValue("version", currentRecord->getVersion());
        conf.setValue("length", currentRecord->getLength());
        conf.setValue("syms_az", currentRecord->getSyms_az() ? "true" : "false");
        conf.setValue("syms_AZ", currentRecord->getSyms_AZ() ? "true" : "false");
        conf.setValue("syms_09", currentRecord->getSyms_09() ? "true" : "false");
        conf.setValue("syms_special", currentRecord->getSyms_special() ? "true" : "false");
        conf.endGroup();
    }
}

void fpwdRecordFactory::removeRecordFromFile(QString _filename, int _id)
{
    QSettings conf(_filename, QSettings::IniFormat);

    conf.beginGroup(QString::number(_id));
    conf.remove("");
    conf.endGroup();
}
